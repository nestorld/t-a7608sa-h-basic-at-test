# THIS IS A VERY BASIC SCRIPT TO TEST AT COMMANDS


############################
#  FOR MODEL T-A7608SA-H   #
############################


from machine import Pin, UART
import network
import urequests
from time import sleep


def send_at(uart, at_cmd, wait_time=0, debug_mode=True):
    
    # Send AT command. Waiting time for response is configurable (seconds).

    uart.read()  # Clean buffer
    print(at_cmd)
    at_cmd = at_cmd + "\r\n"
    at_cmd_bytes = bytes(at_cmd, 'utf-8')
    print(at_cmd_bytes)
    sentBytes = uart.write(at_cmd_bytes)
    print(sentBytes)

    if debug_mode:
        sleep(wait_time)
        data = uart.read()
        if data is None:
            print("...No response")
            return False
        else:
            print(data.decode("utf-8"))
            return True



print("In boot")

modemPowerPin = 12
startUpPin = 4
modemDTRPin = 7
uartToModemTxPin = 18
uartFromModemRxPin = 17
BAUDRATE = 115200

modemPower = Pin(modemPowerPin, Pin.OUT)
modemPower.on()

# Power on the modem respecting the timing for it
# (https://microchip.ua/simcom/LTE/A76xx/A7602/A7602E-H&A7608SA-H%20Hardware%20Design_V1.00.pdf on page 29)
startUp = Pin(startUpPin, Pin.OUT)
startUp.off()
sleep(2.5)
startUp.on()
sleep(2.5)
startUp.off()
sleep(13)

# Initializes the UART
uartModem = UART(1, BAUDRATE, tx=uartToModemTxPin, rx=uartFromModemRxPin, flow=0)
uartModem.init(BAUDRATE, bits=8, parity=None, stop=1)
sleep(1)

# Send AT commands to connect LTE modem until a response is received
while not send_at(uartModem, "ATI", wait_time=2):
    sleep(1)
